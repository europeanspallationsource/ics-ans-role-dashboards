import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_dashboards_containers(Command, Sudo):
    with Sudo():
        cmd = Command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['dashboards', 'kernel-gateway', 'traefik_proxy']


def test_dashboards_index(Command):
    # This tests that traefik forwards traffic to the dashboards server
    # and that we can access the dashboards index page
    cmd = Command('curl -H Host:dashboards.docker.localhost -k -L https://localhost')
    assert 'Jupyter Dashboard' in cmd.stdout
