ics-ans-role-dashboards
=======================

Ansible role to install Jupyter Dashboards Server.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

Role Variables
--------------

```yaml
dashboards_network: dashboards-network
dashboards_kernel_gateway_image: europeanspallationsource/kernel-gateway
dashboards_kernel_gateway_tag: latest
dashboards_image: europeanspallationsource/dashboards-server
dashboards_tag: 0.8.0
dashboards_traefik_host: localhost
dashboards_traefik_frontend_rule: "Host:{{dashboards_traefik_host}}"
dashboards_auth_token: notebook_to_dashboard_secret
dashboards_username: demo
dashboards_password: demo
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-dashboards
```

License
-------

BSD 2-clause
